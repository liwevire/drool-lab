package com.liwevire.model.fact;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AddressCheckResult {
	private boolean postCodeResult = false; // true:Passed the check；false：Failed verification
}