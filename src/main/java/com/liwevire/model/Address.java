package com.liwevire.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Address {
	private String postcode;
	private String street;
	private String state;
}