package com.liwevire.controller;

import javax.annotation.Resource;

import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.liwevire.model.Address;
import com.liwevire.model.fact.AddressCheckResult;

@RequestMapping("/test")
@Controller
public class TestController {

	@Resource
	private KieContainer kieContainer;

	@ResponseBody
	@RequestMapping("/address")
	public void test(int num) {
		KieSession kieSession = kieContainer.newKieSession();

		Address address1 = new Address();
		address1.setPostcode("64646");
		Address address2 = new Address();
		address2.setPostcode("46464");
		AddressCheckResult result1 = new AddressCheckResult();
		AddressCheckResult result2 = new AddressCheckResult();
		
		System.out.println(result1);
		System.out.println(result2);

		kieSession.insert(address1);
		kieSession.insert(address2);
		kieSession.insert(result1);
		kieSession.insert(result2);

		int ruleFiredCount = kieSession.fireAllRules();
		kieSession.destroy();
		System.out.println("Triggered" + ruleFiredCount + "Rules");
//		System.out.println("Result1:" + result1.isPostCodeResult());
//		System.out.println("Result2:" + result2.isPostCodeResult());

	}

	/**
	 * 生成随机数
	 * 
	 * @param num
	 * @return
	 */
	public String generateRandom(int num) {
		String chars = "0123456789";
		StringBuffer number = new StringBuffer();
		for (int i = 0; i < num; i++) {
			int rand = (int) (Math.random() * 10);
			number = number.append(chars.charAt(rand));
		}
		return number.toString();
	}
}
